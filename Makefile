PREFIX:=/usr/local
MANPREFIX:=$(PREFIX)/share/man

CC      := cc
LD      := $(CC)

CFLAGS  += -pedantic -Wall
LDFLAGS +=

BIN = mkb
SRC = mkb.c
MAN = mkb.1

OBJ = $(SRC:.c=.o)

.POSIX:

all: binutils

binutils: $(BIN)

.o:
	@echo "LD $@"
	@$(LD) $< -o $@ $(LDFLAGS)

.c.o:
	@echo "CC $<"
	@$(CC) -c $< -o $@ $(CFLAGS)

install: $(BIN) $(MAN)
	mkdir -p $(DESTDIR)$(PREFIX)/bin/
	cp -f $(BIN) $(DESTDIR)$(PREFIX)/bin/
	chmod 755 $(DESTDIR)$(PREFIX)/bin/$(BIN)
	mkdir -p $(DESTDIR)$(MANPREFIX)/man1/
	cp -f $(MAN) $(DESTDIR)$(MANPREFIX)/man1/
	chmod 644 $(DESTDIR)$(MANPREFIX)/man1/$(MAN)


uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/$(BIN)
	rm -f $(DESTDIR)$(MANPREFIX)/bin/$(MAN)

clean :
	rm -f $(OBJ) $(BIN)
