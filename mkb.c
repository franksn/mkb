#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <err.h>

#define DEFAULT_SIZE 32
#define DEFAULT_CHAR1 "━"
#define DEFAULT_CHAR2 "━"
#define DEFAULT_START " "
#define DEFAULT_END   " "
#define DEFAULT_SEP   "╋"

int
main (int argc, char **argv)
{
	int i;
	float size  = 0;
	float value = 0;
	char *end   = NULL;
	char *sep   = NULL;
	char *char1 = NULL;
	char *char2 = NULL;
	char *begin = NULL;
	char **current = NULL;

	size = getenv("SIZE")   ? atoi(getenv("SIZE")) : DEFAULT_SIZE;
	char1 = getenv("CHAR1") ? getenv("CHAR1") : DEFAULT_CHAR1;
	char2 = getenv("CHAR2") ? getenv("CHAR2") : DEFAULT_CHAR2;
	begin = getenv("START") ? getenv("START") : DEFAULT_START;
	end   = getenv("END")   ? getenv("END")   : DEFAULT_END;
	sep   = getenv("SEP")   ? getenv("SEP")   : DEFAULT_SEP;

	if (argc < 2)
		scanf("%f", &value);
	else
		value = atof(argv[1]);

	if (value > 100)
		errx(1, "value should remain between 0 and 100");

	write(fileno(stdout), begin, strnlen(begin, 32));
	for (i=0; i<size; i++) {
		current = (i < value / 100 * size) ? &char1 : (current == &char1 ? &sep : &char2);
		write(fileno(stdout), *current, strnlen(*current, 32));
	}
	write(fileno(stdout), end, strnlen(end, 32));

	putc('\n', stdout);

	return 0;
}
